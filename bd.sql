-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 19-11-2019 a las 01:02:53
-- Versión del servidor: 10.1.34-MariaDB
-- Versión de PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `testimoniobd`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `testimonio`
--

CREATE TABLE `testimonio` (
  `ID_testimonio` int(10) NOT NULL,
  `nombre` varchar(30) COLLATE utf8_spanish2_ci NOT NULL,
  `descripcion` varchar(200) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `testimonio`
--

INSERT INTO `testimonio` (`ID_testimonio`, `nombre`, `descripcion`) VALUES
(5, 'Alejandro stiven ', 'cacacacacacacacacacac'),
(6, 'Alejandro stiven ', 'Me parece una gran aplicaciÃ³n ya que es de bajo costo y un muy buen rendimiento por esas razones la compraria de manera imediata'),
(7, 'jose', 'Llevar los billetes de viaje en el mÃ³vil en vez de imprimirlos. AÃ±adir productos al carrito de la compra desde casa con sÃ³lo mover el ratÃ³n. Aprender programaciÃ³n con los hijos. Conectar a dos pr'),
(8, 'carlos', 'Hoy en dÃ­a, las posibilidades de aprender, colaborar y crear aumentan a la velocidad de la fibra Ã³ptica: \"Tenemos que acostumbrarnos a que no todo es estable: al revÃ©s. Hoy trabajas de esto y maÃ±a'),
(9, 'issac', ' Google for Work, sabe que en su dÃ­a a dÃ­a de su trabajo puede aportar su granito de arena al mundo gracias a la internet y aconseja que no deberÃ­amos trabajar con tecnologÃ­a mÃ¡s obsoleta que la '),
(10, 'juan', 'La empresaria Usue Madinaveitia empresaria y madre, hizo clic poniendo una almohadilla delante de salpuntual creando un movimiento que alcanzÃ³ una audiencia de 12 millones.\r\n'),
(11, 'david gomez', 'Porque ya no es cuÃ¡nto, ni dÃ³nde, sino -sobre todo-, cÃ³mo: en oficinas compartidas, desde una playa o en la misma terminal, la tecnologÃ­a nos ha brindado la posibilidad de trabajar: aquÃ­, allÃ­, '),
(12, 'blanca gomez', 'directora de RRHH de Microsoft, hace los deberes con sus hijos a travÃ©s de Skype cuando estÃ¡ de viaje: \"OjalÃ¡ hubiese tenido antes las herramientas que he tenido en estos Ãºltimos aÃ±os\", nos confi'),
(13, 'angie sanchez', 'Porque ya no es cuÃ¡nto, ni dÃ³nde, sino -sobre todo-, cÃ³mo: en oficinas compartidas, desde una playa o en la misma terminal, la tecnologÃ­a nos ha brindado la posibilidad de trabajar: aquÃ­, allÃ­, '),
(14, 'pedro', ' En el BBVA lo saben. El futuro se escribe en clave de colaboraciÃ³n, simplicidad y transparencia: \"Queremos crear una nueva cultura de trabajo que fomente la interacciÃ³n -formal e informal- entre nu'),
(15, 'gustavo petro', ' El dÃ­a que Ãlvaro GonzÃ¡lez-Alorda, Co-fundador de Emergap, se quedÃ³ sin trabajo, hizo clic y empezÃ³ construir un nuevo camino sirviÃ©ndose de la tecnologÃ­a.'),
(16, 'camilo', ' El director de Wayra EspaÃ±a (TelefÃ³nica Open Future), AndrÃ©s Saborido, hizo clic cuando vio cÃ³mo unos jÃ³venes con pocos medios llegar a comercializar un audÃ­fono digital para hipoacÃºsicos usan');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `testimonio`
--
ALTER TABLE `testimonio`
  ADD PRIMARY KEY (`ID_testimonio`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `testimonio`
--
ALTER TABLE `testimonio`
  MODIFY `ID_testimonio` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
