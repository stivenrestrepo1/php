<!DOCTYPE html>
<html lang="en">
   <head>
      <title>Nualtec</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- favicon-->
      <link rel="icon" type="image/png" sizes="32x32" href="recursos/image/favicon-32x32.png">
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <link href="recursos/css/responsive.css" rel="stylesheet">
      <link href="recursos/css/animate.css" rel="stylesheet">
      <link rel="stylesheet" href="recursos/css/font-awesome.min.css">
      <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700,800" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="recursos/css/slick-theme.css">
      <link rel="stylesheet" type="text/css" href="recursos/css/style.css">
   </head>
   <body>
      <!--header-->
      <header class="navigation">
       
       <nav class="navbar navbar-expand-lg navbar-light bg-light">
          <div class="container text-center">
             <div class="navbar-header">
               <a class="navbar-brand" href="index.html"><img src="recursos/image/logo.png" alt="logo" style="width: 150px"></a>
             </div>
             <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav menu-left">
                   <li class="nav-item dropdown">
                      <a class="nav-link dropdown-toggle" class="nav-link" data-toggle="dropdown" href="#">Inicio
                      <span class="caret"></span></a>
                      <ul class="dropdown-menu">
                         <li><a href="" class="nav-link">Aplicaciones móviles</a></li>
                         <li><a href="desktop.html" class="nav-link">Aplicaciones de escritorio</a></li>
                         <li><a href="web-app.html" class="nav-link">Web Apps</a></li>
                      </ul>
                   </li>
                   <li><a href="#caracteristicas" class="nav-link">Caracteristicas</a></li>
                   <li><a href="#acercade" class="nav-link">Acerca de</a></li>
                   <li><a href="#precios" class="nav-link">Precios</a></li>
                   <li><a href="#siguenos" class="nav-link">Contacto</a></li>
                </ul>
                <ul class="nav navbar-nav  button-right">
                <button type="button" class="btn btn-danger btn-lg">Obtener Nualtec</button>
                </ul>
             </div>
          </div>
       </nav>
    </header>
      <!-- end header-->
      <!--banner-->
      <section class="mobile-app-banner">
         <div class="container">
            <div class="row">
               <div class="col-sm-6 col-md-6">
                  <div class="mobile-app-banner-content  wow animated fadeInLeft">
                     <h1 class="text-white">Nualtec demo <br> plantilla de ejemplo</h1>
                     <p class="text-white">Esta es una plantilla de ejemplo que se deberá realizar como ejercicio durante la aplicación al puesto de Desarrollador HTML, CSS, JS, PHP y MySql. </p>
                     <div class="buttons ">
                        <button type="button" class="btn btn-danger bt-lg">Iniciar</button>
                        <button type="button" class="btn btn-danger bt-lg">Ver más</button>
                     </div>
                  </div>
               </div>
               <div class="col-sm-6 col-md-6">
                  <div class="mobile-app-banner-image  wow animated zoomIn">
                     <img src="recursos/image/phone-image.png" alt="phone">
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!--end banner-->
      <!--feature-->
      <section class="feature">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <div id="caracteristicas"  class="heading text-center wow animated slideInUp">
                     <h3>Nualtec tiene grandes cosas para explorar</h3>
                     <h3>Checkout features:</h3>
                     <br>
                  </div>
               </div>
               <br>
               <div class="col-md-3 col-sm-6">
                  <div class="card border-dark mb-3">
                     <br>
                  <div class="feature-text text-center  wow animated slideInLeft">
                     <a href=""><img src="recursos/image/mobile.png" alt="logo" style="width: 55px"></a>
                     <br>
                     <br>
                     <h4>Pruebalo gratis</h4>
                     <br>
                     <p>Lorem ipsum dolor sit amet consecs</p>
                     </div>
                     <br>
                     </div>
               </div>
               <br>
               <div class="col-md-3 col-sm-6">
                  <div class="card border-dark mb-3">
                     <br>
                  <div class="feature-text text-center wow animated slideInLeft">
                  <a href=""><img src="recursos/image/hand.png" alt="logo" style="width: 55px"></a>
                     <br>
                     <br>
                     <h4>Pagos seguros</h4>
                     <br>
                     <p>Tetur sadispiching elit lorem amet consecs Lorem i</p>
                     </div>
                     <br>
                  </div>
               </div>
               <br>
               <div  class="col-md-3 col-sm-6">
                  <div class="card border-dark mb-3">
                     <br>
                  <div class="feature-text text-center wow animated slideInRight">
                  <a href=""><img src="recursos/image/desktop.png" alt="logo" style="width: 55px"></a>
                  <br>
                  <br>
                     <h4>Sin instalación</h4>
                     <br>
                     <p>Class aptent  is not conub rep tosr sadispiching elit lorem amet</p>
                     <br>
                     </div>
                  </div>
               </div>
               <br>
               <div class="col-md-3 col-sm-6">
                  <div class="card border-dark mb-3">
                     <br>
                  <div class="feature-text text-center wow animated slideInRight">
                  <a href=""><img src="recursos/image/girl.png" alt="logo" style="width: 55px"></a>
                  <br>
                  <br>
                     <h4>Soporte 24h</h4>
                     <br>
                     <p>Class aptent  is not conub rep tosonub rep tosr sadispiching </p>
                     <br>
                     </div>
                  </div>
               </div>
               <br>
               <div class="col-md-3 col-sm-6">
                  <div class="card border-dark mb-3">
                     <br>
                  <div class="feature-text text-center wow animated slideInLeft">
                  <a href=""><img src="recursos/image/mobile-lock.png" alt="logo" style="width: 55px"></a>
                     <br>
                     <br>
                     <h4>Acceso rapido</h4>
                     <br>
                     <p>Class aptent  is not conub rep tosonub rep tosonub reps</p>
                     </div>
                     <br>
                  </div>
               </div>
               <br>
               <div class="col-md-3 col-sm-6">
                  <div class="card border-dark mb-3">
                     <br>
                  <div class="feature-text text-center wow animated slideInLeft">
                  <a href=""><img src="recursos/image/pin-map.png" alt="logo" style="width: 55px"></a>
                     <br>
                     <br>
                     <h4>Seguimiento</h4>
                     <br>
                     <p>Class aptent  is not conub rep tosnot conub rep tosonub </p>
                     <br>
                     </div>
                  </div>
               </div>
               <br>
               <div class="col-md-3 col-sm-6">
                  <div class="card border-dark mb-3">
                     <br>
                  <div class="feature-text text-center wow animated slideInRight">
                  <a href=""><img src="recursos/image/person.png" alt="logo" style="width: 55px"></a>
                     <br>
                     <br>
                     <h4>Administrador</h4>
                     <br>
                     <p>Class aptent  is not conub rep tosnot conub rep tosonub </p>
                     <br>
                     </div>
                  </div>
               </div>
               <br>
               <div class="col-md-3 col-sm-6">
                  <div class="card border-dark mb-3">
                     <br>
                  <div class="feature-text text-center wow animated slideInRight">    
                  <a href=""><img src="recursos/image/mobile1.png" alt="logo" style="width: 55px"></a>
                  <br>
                  <br>
                     <h4>Actualizaciones</h4>
                     <br>
                     <p>Class aptent  is not conub rep tostosnot conub rep t</p>
                     <br>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!--end feature-->
      <!--video-->
      <br>
      <br>
      <br>
      <section class="video">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <div class="heading text-center wow animated slideInUp">
                     <h3>Haz que tu aplicación sea más Nualtec!</h3>
                     <h3>Tutoriales disponibles:</h3>
                  </div>
                  <br>
                  <br>
                    <div class="overlay-image wow animated zoomIn"> 
                    <div class="embed-responsive embed-responsive-4by3">
                  <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/zpOULjyy-n8?rel=0" allowfullscreen></iframe>
                  </div> 
                   </div>
               </div>
            </div>
         </div>
         <br>
         <br>
         <br>
      </section>
      <!--end video-->
      <!--work-->
      <section class="work">
         <div class="container">
            <div class="row">
               <div class="col-md-12 ">
                  <div id="acercade" class="heading text-center wow animated slideInUp">
                     <h3>Nualtec app es fácil de usar!</h3>
                     <h3>Así funciona</h3>
                  </div>
                  <br>
                  <br>
               </div>
               <div class="col-md-6">
                  <div class="work-text wow animated slideInLeft">
                     <img src="recursos/image/work2.png" class="img-responsive" alt="work">
                     <br>
                     <br>
                     <br>
                     <h4><img src="recursos/image/download.png" alt="download"> Descargar</h4>
                     <br>
                     <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</p>
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="work-text wow animated slideInRight">
                     <img src="recursos/image/work1.png" class="img-responsive" alt="work">
                     <br>
                     <br>
                     <br>
                     <h4><img src="recursos/image/check.png" alt="check">Pruébalo gratis</h4>
                     <br>
                     <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</p>
                  </div>
               </div>
               <div class="col-md-12">
                  <div class="buttons text-center wow animated bounce">
                     <br>
                     <br>
                  <button type="button" class="btn btn-danger bt-lg">Probar gratis</button>
                  <button type="button" class="btn btn-danger bt-lg">Conocer más</button>
                  </div>
               </div>
            </div>
         </div>
         <br>
         <br>
         <br>
      </section>
      <!--end work-->
      <!--question-->
      <section class="question">
         <div class="container">
            <div class="row">
               <div class="col-md-12 ">
                  <div id="precios" class="heading text-center wow animated slideInUp">
                     <h3>¿Tienes preguntas?</h3>
                     <h3>tenemos las respuestas:</h3>
                  </div>
                  <br>
                  <br>
               </div>
               <div class="col-md-4">
                  <div  class="card text-align" class="quest-text wow animated slideInLeft" >
                     <h4 class="card-title">¿Como funciona la app?<span class="pull-right purple "><i class="	fa fa-question"></i></span></h4>
                     <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem doloremque et minima totam nesciunt beatae qui, vel tempore sint similique voluptatibus! In corrupti, est perspiciatis aliquam quibusdam maxime fugit illo..</p>
                     </div>
               </div>
               <div class="col-md-4">
                  <div class="card text-align" class="quest-text wow animated slideInUp" >
                     <h4 class="card-title">¿Es Nualtec segura? <span class="pull-right yellow"><i class="	fa fa-question"></i></span></h4>
                     <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. At eveniet distinctio error ab id tenetur voluptatum, neque officiis nostrum ad in, sint quaerat ex, repudiandae inventore magnam dicta qui ratione..</p>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="card text-align" class="quest-text wow animated slideInRight" >
                     <h4 class="card-title">¿Cuál es el costo? <span class="pull-right pink"><i class="	fa fa-question"></i></span></h4>
                     <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt esse perspiciatis corrupti hic nostrum cupiditate unde delectus, vel suscipit, ut molestiae autem fugiat iste earum in harum, eligendi nam. Dignissimos?.</p>
                  </div>
               </div>
            </div>
         </div>
         <br>
         <br>
         <br>
         <br>
         <br>
      </section>
      <!--end question-->
      <!--testimonial-->
      <section class="testimonial">
         <div class="container">
            <div class="row">
               <div id="testimonios"class="col-md-12 ">
                  <div class="heading text-center">
                     <h3>Testimonios de nuestros usuarios</h3>
                     <h3> Nuestros clientes dicen.</h3>
                  </div>
                  <br>
                  <br>
               </div>
            </div>
            <div class="row">
               <div class="col-md-12 no-padding">
               
               <?php

                  include dirname(__file__,2).'/modelo/testimonioModel.php';

                  $testimonio  = new Testimonio();


                   //Trae todos los productos

                   $testimonio =$testimonio->getTestimonio();
                  
                 

                  $total=count($testimonio);

                  $articulos_x_pagina = 3;

                   $paginas = $total/$articulos_x_pagina;


                  $title="Listado de testimonios";
                  ?>
   <div>
      <nav arial-label="Page navigation example">
         <ul class="pagination">
                     <li class="page-item">
                        <a class="page-link" 
                        href="home.php?pagina=<?php echo $_GET['pagina']-1 ?>">Anterior</a>
                     </li>
                     
                     <?php for ($i=0; $i<$paginas;$i++): ?>

                     <li class="page-item
                     <?php echo $_GET['pagina']==$i+1  ? 'active':''  ?>"><a class="page-link" href="home.php?pagina=<?php echo $i+1 ?>"><?php echo $i+1 ?></a></li>

                     <?php endfor?>
         
                     <li class="page-item <?php echo $_GET['pagina']>=$paginas  ? 'disabled':''  ?>">
                        <a class="page-link"
                         href="home.php?pagina=<?php echo $_GET['pagina']+1 ?>">Siguiente</a></li>
          </ul>
      </nav>
   </div>


<div class="table-responsive">
		<table class="table table-striped table-bordered table-hover">
			<thead class="thead-dark">
				<th class="text-center">ID</th>
				<th class="text-center">Nombre</th>
            <th class="text-center">Descripcion</th>
			</thead>
			<tbody>
				<?php

					if(count($testimonio)>0){

						foreach ($testimonio as $column =>$value) {
				?>

                     <tr class="text-center" id="row<?php echo $value['ID_testimonio']; ?>">
                     <td ><?php echo $value['ID_testimonio']; ?></td>
								<td><?php echo $value['nombre']; ?></td>
								<td><?php echo $value['descripcion']; ?></td>
									
								
							</tr>
				<?php
						}
					}else{
				?>
					<tr>
						<td colspan="5">
							<div class="alert alert-info">
								No se encontraron testimonios.
							</div>
						</td>
					</tr>
				<?php
					}
				?>
			</tbody>
		</table>
	</div>



      </section>
      <br>



      <div class="container">
            <div class="form-row">
               <div class="col-md-4">
                  <div  class="text-center" >
                     </div>
               </div>
               <div class="card mx-xl-5">
                  <div class="card-body" class="quest-text wow animated slideInUp" >
                     
            <form id="newTestimonio"action="../controlador/testimonioController.php" method="POST">
            <h6 class="text-center">Nuevo testimonio</h6>
            <div class="form-group">
               <div class="col-md-4" >
                   <div class="form-group">
                      <label for="nombre" class="font-weight-light">Nombre</label>
                      <input type="text" class="" id="nombre" name="nombre"  placeholder="Ingrese el nombre" require>
               </div>
               <div class="form-group">
                   <label for="descripcion">Descripcion</label>
                     <textarea class="" id="descripcion" name="descripcion" rows="5" placeholder="Describa el testimnio"></textarea require>
               </div>
                     <button type="submit" name="create" value="Crear" class="btn btn-primary">Enviar</button>
               </div>
               <?php
  		                  if(isset($_GET['success'])){
	            ?>
			                  <div class="alert alert-success">
				                    El testimonio ha sido registrada.
			                  </div>
	            <?php
  		                  }else if(isset($_GET['error'])){
  	            ?>
			                  <div class="alert alert-danger">
				                     Ha ocurrido un error al crear el testimonio, por favor intente de nuevo.
			                  </div>
	            <?php
  		                  }
  	            ?>
               </div> 
            </form>

                  </div>
               </div>
               <div class="col-md-4">
               </div>
            </div>
         </div>
         <br>
         <br>
         <br>

      <!--end testimonial-->
      <!--trust-->
      <section class="trust">
         <div class="container">
            <div class="row">
               <div class="col-md-12 ">
                  <div class="heading text-center wow animated slideInUp ">
                     <h3>Nuestros clientes</h3>
                  </div>
                  <br>
                  <br>
                  <div class="responsive2">
                     <div> <img  src="recursos/image/logo-icon1.png" align="left" alt="logo-icon"></div>
                     <div> <img src="recursos/image/logo-icon2.png" align="left"  alt="logo-icon"></div>
                     <div> <img src="recursos/image/logo-icon3.png" align="left" alt="logo-icon"></div>
                     <div> <img src="recursos/image/logo-icon4.png" align="left" alt="logo-icon"></div>
                     <div> <img src="recursos/image/logo-icon5.png" align="left" alt="logo-icon"></div>
                     <div> <img src="recursos/image/logo-icon6.png" align="left" alt="logo-icon"></div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!--end trust-->
      <!--footer-->
      <section class="footer">
         <div class="container">
            <div class="row">
               <div class="col-md-12 ">
               <br>
               <br>
               <br>
               <br>
                  <div id="siguenos" class="heading text-center wow animated slideInUp ">
                     <h3>Síguenos</h3>
                     <h3>No te pierdas nuestras actualizaciones:</h3>
                  </div>
                  <br>
               <br>
               <br>
               <br>
               </div>
               <div class="col-md-4">
                  <div class="social facebook text-center wow animated slideInLeft">
                     <h4>¡Siguenos en facebook!</h4>
                     <p class="color-p">254,466 likes</p>
                     <p class="fb">
                        <a href=""><span><i class="	fa fa-facebook-f"></i></span> facebook.com</a>
                     </p>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="social youtube text-center wow animated bounce">
                     <h4>¡Siguenos en facebook!</h4>
                     <p class="color-p">258,466 Subscriber</p>
                     <p class="ytb">
                        <button class="btn btn-danger bt-lg" ><i class="fa fa-youtube-play"></i>Youtube.com</button>
                     </p>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="social twitter text-center wow animated slideInRight">
                     <h4>¡Siguenos en facebook!</h4>
                     <p class="color-p">258,466 Followers</p>
                     <p class="twitt">
                        <a href="" ><span><i class="	fa fa-twitter"></i></span> Twitter.com</a>
                     </p>
                  </div>
               </div>
               <div class="clear-fix"></div>
               <div class="col-md-12">
                  <hr class="hrline">
               </div>
            </div>
         </div>
      </section>
      <!--end footer-->
      <!--copyright-->
      <footer class="footer-copyright">
         <div class="container">
            <div class="row">
               <div class="col-md-12">
                  <div class="playstore wow animated slideInUp">
                     <h2><b>Descarga Nualtec hoy</b></h2>
                     <p>y pruébalo gratis por un mes</p>
                     <a href="#" class="read app-store">
                        <i class="fa fa-apple apple" aria-hidden="true"></i>
                        <p><span>descarga desde </span><br>Apple Store </p>
                     </a>
                     <a href="#" class="read app-store mleft">
                        <i class="	fa fa-play apple" aria-hidden="true"></i>
                        <p><span>descarga desde</span><br>Google play </p>
                     </a>
                  </div>
               </div>
            </div>
            <div class="footer-section-back">
               <div class="row">
                  <div class="col-md-6 col-sm-6 col-xs-5">
                     <div class="footer-logo wow animated slideInLeft" >
                        <img src="recursos/image/logo-footer.png" alt="footer-logo" style="width: 150px">
                     </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-7">
                     <div class="footer-text wow animated text-right slideInRight">
                        <p>© 2019 Todos los derechos resevados.<a href="#"> Nualtec</a></p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </footer>
      <!--end copyright-->
      <!--jQuery-->
     
       <!--Bootstrap-->
    
       <!--wow js-->
     
       <!--Slick-->


      <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
   </body>
</html>
