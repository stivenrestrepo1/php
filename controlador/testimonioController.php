<?php
	include dirname(__file__,2).'/modelo/testimonioModel.php';
	

	$testimonios = new Testimonio();

	//Request: creacion de un nuevo testimonio
	if(isset($_POST['create']))
	{
		if($testimonios->newTestimonio($_POST)){
			header('location: ../vista/home.php?page=newTestimonio&success=true');
		}else{
			header('location: ../vista/home.php?page=newTestimonio&error=true');
		}
	}

	
	//Request: eliminar testimonio
	if(isset($_GET['delete']))
	{
		if($testimonios->deleteTestimonio($_GET['ID_testimonio'])){
			echo json_encode(["success"=>true]);
		}else{
			echo json_encode(["error"=>true]);
		}
	}

?>